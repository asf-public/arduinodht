#ifndef DISPLAYMODE_H
#define DISPLAYMODE_H


#include <LiquidCrystal.h>
#include "reading.h"

class DisplayMode {
  char dhtStatus;
  unsigned long lastStatusUpdate = 0;
  unsigned long statusInterval;
  void clearStatus();
  protected:
    LiquidCrystal* lcd;
    Reading* reading;
    const char degreeChar = 0;
  public:
    /**
    * Constructor.
    * @param lcd pointer to a LiquidCrystal object.
    * @param reading a pointer to a Reading object
    * @param statusInterval interval in milliseconds for the status symbol
    */
    DisplayMode(LiquidCrystal* lcd, Reading* reading, unsigned long statusInterval) : 
      lcd(lcd), reading(reading), statusInterval(statusInterval) {}
    /**
    * Initialized the liquid crystal library and prints initial display message.
    * @param message
    * @param version
    */
    void init(const char*, const char*);
    void updateDisplay();
    void clear();
    void printStatus();
    void mayClearStatus();
    virtual void printTemplate() = 0;
    virtual void printValues() = 0;
};

#endif
