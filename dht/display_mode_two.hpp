#include "display_mode.hpp"

class DisplayModeTwo: public DisplayMode {
  public:
    /**
    * Constructor.
    * @param lcd pointer to a LiquidCrystal object.
    * @param reading a pointer to a Reading object
    * @param statusInterval interval in milliseconds for the status symbol
    */
    DisplayModeTwo(LiquidCrystal* lcd, Reading* reading, unsigned long statusInterval) : DisplayMode(lcd, reading, statusInterval) {} 
    void printTemplate();
    void printValues();
};
