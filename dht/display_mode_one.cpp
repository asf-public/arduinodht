#include "display_mode_one.hpp"

void DisplayModeOne::printTemplate() {
  lcd->setCursor(0, 0);
  lcd->print("    T:     ");
  lcd->print(degreeChar);
  lcd->print("C  ");

  lcd->setCursor(0, 1);
  lcd->print("    H:      %    ");
}

void DisplayModeOne::printValues() {
  char str[5];
  lcd->setCursor(7, 0);
  dtostrf(reading->temperature, 4, 1, str);
  lcd->print(str);

  lcd->setCursor(7, 1);
  dtostrf(reading->humidity, 4, 1, str);
  lcd->print(str);
}
