
#ifndef READING_H
#define READING_H

/**
 * Sensor data.
 * @param temperature temperature in degrees Celsius
 * @param humidity relative humidity in percentage
 * @param hic
 * @param chk whether reading was successful
 */
struct Reading {
  float temperature;
  float humidity;
  float hic;
  bool chk;
};

#endif 
