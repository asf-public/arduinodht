#include "dht_reader.hpp"

void DHTReader::init() {
  dht->begin();
}

bool DHTReader::read(bool computeHic) {
  currentMillis = millis();
  if (currentMillis - previousMillis >= readInterval) {
    previousMillis = currentMillis;
    
    reading->humidity = dht->readHumidity();
    reading->temperature = dht->readTemperature();

    if (isnan(reading->humidity) || isnan(reading->temperature)) {
      reading->chk = false;
    } else {
      reading->chk = true;
      if (computeHic) {
        reading->hic = dht->computeHeatIndex(reading->temperature, reading->humidity, false);
      }
    }

    return true;
  } else {
    return false;
  }

}
