#include "display_mode_one.hpp"
#include "display_mode_two.hpp"
#include "dht_reader.hpp"

#define DEBUG 0

#define RS_PIN 12
#define EN_PIN 11
#define D4_PIN 10
#define D5_PIN 9
#define D6_PIN 8
#define D7_PIN 7

#define DHTPIN 6

//#define DHTTYPE DHT11
#define DHTTYPE DHT22
#define READ_INTERVAL 5000
#define STATUS_INTERVAL 1000

#define BL_INPUT_PIN 3
#define BL_OUTPUT_PIN 5
#define BL_FIX_LIMIT 500
#define VALID_PRESSED_INTERVAL 500
#define MD_INPUT_PIN 2

#define VERS "v1.1.0"

struct Reading reading = {0.0f, 0.0f, 0.0f};

DHTReader dhtReader(DHTPIN, DHTTYPE, &reading, READ_INTERVAL);

LiquidCrystal lcd(RS_PIN, EN_PIN, D4_PIN, D5_PIN, D6_PIN,D7_PIN);
DisplayMode* displayModeOne = new DisplayModeOne(&lcd, &reading, STATUS_INTERVAL);
DisplayMode* displayModeTwo = new DisplayModeTwo(&lcd, &reading, STATUS_INTERVAL);
DisplayMode* currentMode = displayModeOne;

unsigned long blPreviousMillis = BL_FIX_LIMIT;

long mdLastPressed = 0;

void setup() {
  #ifdef DEBUG
    Serial.begin(9600);
    Serial.println(F("DEBUGGING"));
  #endif
  dhtReader.init();
  pinMode(BL_INPUT_PIN, INPUT_PULLUP);
  pinMode(BL_OUTPUT_PIN, OUTPUT);
  pinMode(MD_INPUT_PIN, INPUT_PULLUP);
  digitalWrite(BL_OUTPUT_PIN, HIGH);
  currentMode->init("INICIALITZANT...", VERS);
  delay(1000);
  currentMode->clear();
  currentMode->printTemplate();
  attachInterrupt(digitalPinToInterrupt(BL_INPUT_PIN),blButtonPressed,CHANGE);
  attachInterrupt(digitalPinToInterrupt(MD_INPUT_PIN),mdButtonPressed,FALLING);
}

void loop() {

  if (dhtReader.read(currentMode == displayModeTwo)) {
    currentMode->updateDisplay();
  }
  currentMode->mayClearStatus();  
}


void blButtonPressed() {
  #ifdef DEBUG
    Serial.print("BL PRESSED ");
  #endif
  if (digitalRead(BL_INPUT_PIN) == LOW) {
    blPreviousMillis = millis();
    digitalWrite(BL_OUTPUT_PIN, HIGH);
  } else {
    if (millis() - blPreviousMillis < BL_FIX_LIMIT) {
      digitalWrite(BL_OUTPUT_PIN, LOW);
    }
    blPreviousMillis = 0;
  }
}

void mdButtonPressed() {
  #ifdef DEBUG
    Serial.println("MD PRESSED");
  #endif
  if (validSignal( &mdLastPressed)) {
    if (currentMode == displayModeOne) {
      currentMode = displayModeTwo;
    } else {
      currentMode = displayModeOne;
    }
    currentMode->clear();
    currentMode->printTemplate();
    currentMode->printValues();
  }
}

bool validSignal(long *lastPressed) {
  long now = millis();
  if (now - *lastPressed > VALID_PRESSED_INTERVAL) {
    *lastPressed = now;
    return true;
  }
  return false;
}
