#include "display_mode_two.hpp"

void DisplayModeTwo::printTemplate() {
  lcd->setCursor(1, 0);
  lcd->print("T:     ");
  lcd->print("/     ");
  lcd->print(degreeChar);
  lcd->print("C");

  lcd->setCursor(0, 1);
  lcd->print("    H:      %    ");
}

void DisplayModeTwo::printValues() {
  char str[5];
  lcd->setCursor(3, 0);
  dtostrf(reading->temperature, 4, 1, str);
  lcd->print(str);
  lcd->setCursor(10, 0);
  dtostrf(reading->hic, 4, 1, str);
  lcd->print(str);

  lcd->setCursor(7, 1);
  dtostrf(reading->humidity, 4, 1, str);
  lcd->print(str);
}
