
#include "reading.h"
#include <DHT.h>

/**
 Encapsulates DHT sensor reading.

 @author Agustí Sánchez
 */
 
class DHTReader {
  DHT* dht;
  Reading* reading;
  unsigned long readInterval;
  unsigned long previousMillis = 0;
  unsigned long currentMillis = 0;
  public:
    /**
    * Constructor.
    * @param dhtPin the Arduino pin connected to te sensor data line
    * @param dhtType type of DHT sensor (HDT11, DHT22, ...)
    * @param reading a pointer to a Reading object
    * @param readInterval the reading interval in milliseconds
    */
    DHTReader(int dhtPin, int dhtType, Reading* reading, unsigned long readInterval) : 
      dht(dht), reading(reading), readInterval(readInterval), previousMillis(-readInterval) {
        dht = new DHT(dhtPin, dhtType);
        }
    /**
     * Initializes the DHT library.
     */
    void init();
    /**
     * Reads the sensor output into the Reading pointer given in the constructor.
     * @param whether to compute HIC value.
     */
    bool read(bool);
  
};
