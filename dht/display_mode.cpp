
#include "display_mode.hpp"
#include <Arduino.h>

typedef uint8_t byte;

  byte degreeCharMapping[] = {
    0b00011,
    0b00011,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00
  };

  byte statusCharMapping[] = {
    0b11000,
    0b11000,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00,
    0x00
  };

  const char statusChar = 1;  

void DisplayMode::init(const char* text, const char* version) {
  lcd->begin(16, 2);
  lcd->clear();
  lcd->createChar(degreeChar, degreeCharMapping);
  lcd->createChar(statusChar, statusCharMapping);
  lcd->setCursor(0, 0);
  lcd->print(text);
  lcd->setCursor(0, 1);
  lcd->print(version);
}

void DisplayMode::updateDisplay() {

  dhtStatus = (reading->chk ? statusChar  : 'F');
  printStatus();

  if (reading->chk) {
    printValues();
  }
}


void DisplayMode::clear() {
  lcd->clear();
}

void DisplayMode::printStatus() {
  lastStatusUpdate = millis();
  lcd->setCursor(0, 0);
  lcd->print(dhtStatus);
}

void DisplayMode::mayClearStatus() {
    if (dhtStatus != ' ' && millis() - lastStatusUpdate >= statusInterval) {
      clearStatus();
    }
}

void DisplayMode::clearStatus() {
  dhtStatus = ' ';
  printStatus();
}
