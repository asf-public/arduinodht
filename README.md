# ArduinoDHT

This project is the C++ code to make an Arduino Nano microcontroller display the temperature and humidity readings from a DHT11 sensor on a 16x2 LCD display.

The code implements two display modes. `DisplayModeOne` displays temperature and humidity. `DisplayModeTwo` displays the HIC temperature along the regular temperature in addition to the humidity reading.

This project served as a PoC to test OO concepts in C++.

